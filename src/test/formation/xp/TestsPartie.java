package formation.xp;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestsPartie extends TestCase {
    private List<Joueur> joueurs = new ArrayList<>();
    private Partie p;
    private float blindInitiale = 50;
    private float incrementBlindParTour = 10;

    @Before
    public void setUp() {
        joueurs.add(new Joueur("toto", 5000));
        joueurs.add(new Joueur("titi", 5000));

        p = new Partie(joueurs, blindInitiale, incrementBlindParTour);
    }

    @Test
    public void testJoueursInitialises() {
        assertEquals(p.joueurs.size(), 2);
    }

    @Test
    public void testInitialBlindPosition() {
        p.beginTurn();
        p.endTurn();
        assertTrue(p.joueurs.get(0).getBlind() == HasBlind.BIG && p.joueurs.get(1).getBlind() == HasBlind.SMALL);
    }

    @Test
    public void testBlindPositionAfter1Turn() {
        p.beginTurn();
        p.endTurn();
        p.beginTurn();
        p.endTurn();
        assertTrue(p.joueurs.get(1).getBlind() == HasBlind.BIG && p.joueurs.get(0).getBlind() == HasBlind.SMALL);
    }

    @Test
    public void testBlindPositionAfter2Turn() {
        p.beginTurn();
        p.endTurn();
        p.beginTurn();
        p.endTurn();
        p.beginTurn();
        p.endTurn();
        assertTrue(p.joueurs.get(0).getBlind() == HasBlind.BIG && p.joueurs.get(1).getBlind() == HasBlind.SMALL);
    }

    @Test
    public void testBlindAmount() {
        p.beginTurn();
        p.endTurn();
        assertEquals(p.blind, blindInitiale+incrementBlindParTour);
    }
}
