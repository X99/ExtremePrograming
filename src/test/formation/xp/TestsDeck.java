package formation.xp;

import junit.framework.TestCase;
import org.junit.Test;

public class TestsDeck extends TestCase {

    /*Teste si une carte, une fois enlevée, n'est plus dans le deck*/
    @Test
    public void testRetraitCarte() {
        Deck d = new Deck();
        Carte c = null;
        try {
            c = d.giveRandomCard();
        } catch (NoMoreCardsException e) {
            System.out.println(e.getMessage());
        }
        assertFalse(d.cartes.contains(c) == true);
    }

    /*Teste que la taille du Deck a bien diminué d'1 après le retrait d'une carte*/
    @Test
    public void testRetraitCarte2() {
        Deck d = new Deck();
        int tailleAvant = d.cartes.size();
        try {
            Carte c = d.giveRandomCard();
        } catch (NoMoreCardsException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(d.cartes.size(), tailleAvant-1);
    }

    /*Teste que la méthode renvoie une erreur lorsqu'il n'y a plus de cartes*/
    @Test
    public void testPlusDeCartes() {
        Deck d = new Deck();
        for (int i = 0; i < 52; i++) {
            try {
                d.giveRandomCard();
            } catch (NoMoreCardsException e) {
                System.out.println(e.getMessage());
            }
        }

        try {
            d.giveRandomCard();
            fail("L'exception n'a pas été lancée!!");
        } catch (NoMoreCardsException e) {
            assert true;
        }
    }

    /*Teste le générateur de nmobres aléatoires*/
    @Test
    public void testRandomNumberGenerator() {
        Deck d = new Deck();

        for (int i = 0; i < 1000000; i++) {
            int n = d.randomInt(0, 100);
            assertTrue(n >= 0 && n <= 100);
        }
    }

}
