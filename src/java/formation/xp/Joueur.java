package formation.xp;

import java.util.ArrayList;
import java.util.List;

enum HasBlind {BIG, SMALL, NOPE};

public class Joueur {
    private String nom;
    private List<Carte> cartes = new ArrayList<Carte>();
    private float argent;
    private HasBlind blind = HasBlind.NOPE;


    public HasBlind getBlind() {
        return blind;
    }

    public void setBlind(HasBlind blind) {
        this.blind = blind;
    }

    public Joueur(String nom, float argent) {
        this.nom = nom;
        this.argent = argent;
    }

    public String getNom() {
         return nom;
    }

    public void addCarte(Carte carte) {
        cartes.add(carte);
    }


}
