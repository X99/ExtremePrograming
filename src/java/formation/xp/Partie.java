package formation.xp;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Partie {
    protected List<Joueur> joueurs = new ArrayList<>();
    protected int blindPosition = 0;
    protected float blind;
    protected float smallBlind() {
        return blind/2;
    }
    protected float incrementBlindParTour;
    private int tour;
    Deck d = new Deck();
    List<Carte> table = new ArrayList<>();


    public Partie(List<Joueur> joueurs, float blindInitiale, float incrementBlindParTour) {
        blind = blindInitiale;
        this.incrementBlindParTour = incrementBlindParTour;
        this.joueurs = joueurs;
    }

    public void beginTurn() {

        //donne les cartes aux joueurs
        joueurs.forEach(joueur -> {
            try {
                joueur.addCarte(d.giveRandomCard());
                joueur.addCarte(d.giveRandomCard());
            } catch (NoMoreCardsException e) {
                System.out.println(e.getMessage());
            }
        });


        //répartition des blindes
        joueurs.get(blindPosition).setBlind(HasBlind.BIG);
        joueurs.get(blindPosition == (joueurs.size() - 1) ? 0 : blindPosition + 1).setBlind(HasBlind.SMALL);

        //met 3 cartes sur la table
        try {
            table.add(d.giveRandomCard());
            table.add(d.giveRandomCard());
            table.add(d.giveRandomCard());
        } catch (NoMoreCardsException e) {
            System.out.println("Plus de cartes!!");
        }
    }

    //mises
    public void bet() {
    }

    //flop & river
    public void oneMoreCard() {
        //poste 1 carte en plus sur la table
        try {
            table.add(d.giveRandomCard());
        } catch (NoMoreCardsException e) {
            System.out.println("Plus de cartes!!");
        }
    }

    //fin du tour
    public void endTurn() {
        blind += incrementBlindParTour;
        blindPosition = (blindPosition == (joueurs.size()-1) ? 0:blindPosition+1);
    }
}
