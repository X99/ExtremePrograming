package formation.xp;

import org.junit.Test;

import junit.framework.TestCase;

public class TestsCartes extends TestCase {
	@Test
	public void testConstructeurCarte() {
		Carte c = new Carte(Carte.Valeur.AS, Carte.Figure.TREFLE);
		assertEquals(c.getFigure(), Carte.Figure.TREFLE);
		assertEquals(c.getValeur(), Carte.Valeur.AS);
	}
}
