package formation.xp;

public class NoMoreCardsException extends Exception {
    public NoMoreCardsException(String message) {
        super(message);
    }
}
