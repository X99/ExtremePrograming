package formation.xp;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		int nJoueurs = Integer.parseInt(getInputFromUser("Nombre de joueurs? "));

		if (nJoueurs < 2) {
			System.out.println("Nombre de joueurs trop faible!");
			return;
		}

		List<Joueur> joueurs = new ArrayList<>();
		//Création des joueurs
		for (int idx = 0; idx < nJoueurs; idx++) {
			String name = getInputFromUser("Nom du joueur " + (idx + 1) + " ?");
			Joueur j = new Joueur(name, 5000);
			joueurs.add(j);
		}


		//Création de la partie
		Partie p = new Partie(joueurs, 50, 10);

		while(true) {
			p.beginTurn();

			//demander les paris aux utilisateurs

			p.bet();
			p.oneMoreCard();
			p.bet();
			p.oneMoreCard();
			p.endTurn();
		}
	}

	public ArrayList<Float> askForBets(ArrayList<Joueur> joueurs) {
		List<Float> bets = new ArrayList<>();

		for (int i = 0; i < joueurs.size(); i++) {
			int bet = Integer.parseInt(getInputFromUser("Mise pour "+joueurs.get(i).getNom()+" ?"));

			//vérifier la mise (>0, <argent du joueur...)
		}
	}

	public static String getInputFromUser(String message) {
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.println(message);
		String s = reader.nextLine(); // Scans the next token of the input as an int.

		return s;
	}
}
