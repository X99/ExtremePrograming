package formation.xp;


public class Carte {
    public enum Valeur {AS, DEUX, TROIS, QUATRE, CINQ, SIX, SEPT, HUIT, NEUF, DIX, VALET, DAME, ROI};
    public enum Figure {PIQUE, TREFLE, COEUR, CARREAU};

    private Valeur valeur;
    private Figure figure;

    public Valeur getValeur() {
        return valeur;
    }

    public Figure getFigure() {
        return figure;
    }

    public Carte(Valeur valeur, Figure figure) {
        this.valeur = valeur;
        this.figure = figure;
    }
}
