package formation.xp;

import formation.xp.Carte.Valeur;
import formation.xp.Carte.Figure;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Deck {
    protected List<Carte> cartes = new ArrayList<>();

    public Deck() {
        for(Valeur v : Valeur.values()) {
            for(Figure f: Carte.Figure.values()) {
                cartes.add(new Carte(v, f));
            }
        }
    }

    public Carte giveRandomCard() throws NoMoreCardsException {
        if (cartes.size() == 0) {
            throw new NoMoreCardsException("Plus de cartes!!");
        }

        int n = randomInt(0, cartes.size()-1);
        Carte carte = cartes.get(n);
        cartes.remove(n);

        return carte;
    }

    protected int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
